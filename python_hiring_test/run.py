"""Main script for generating output.csv."""

import pandas as pd
import numpy as np
pd.options.mode.chained_assignment = None

# Define the function to calculate all the Stats
def getAVG(row):
    return np.around(float(row['H'])/row['AB'],3)
def getOBP(row):
    return np.around(float(row['H'] + row ['BB'] + row['HBP'])/(row['AB'] + row['BB'] + row['HBP'] + row['SF']),3)
def getSLG(row):
    return np.around(float(row['TB'])/row['AB'],3)
def getOPS(row):
    return np.around(float(row['H'] + row ['BB'] + row['HBP']) / (row['AB'] + row['BB'] + row['HBP'] + row['SF'])
                 + float(row['TB']) / row['AB'], 3)

def main():
    # Create a dataframe from the input CSV.
    df = pd.read_csv('./data/raw/pitchdata.csv')

    # Create dictionary for all split-stat possibilities that we want to compute.
    # This will help to eliminate repeatitive computation and will save time.
    combination_stat_lookup = {}

    # Create a list to store all the results.
    collection_df = []

    # Create a dictionary lookup for getting column header and value based on query.
    split_lookup = {
        'vs LHH': ['HitterSide', 'L'],
        'vs RHH': ['HitterSide', 'R'],
        'vs LHP': ['PitcherSide', 'L'],
        'vs RHP': ['PitcherSide', 'R']
    }

    # Read the combination file and save it as plain text.
    with open('./data/reference/combinations.txt') as f:
        # Ignore the header
        lines = f.readlines()[1:]

    # Iterate through all the lines.
    for line in lines:

        # Retrive information from a given line.
        combination_string = line.rstrip('\n')
        stat, subject, split_conditon = combination_string.split(',')

        # Create a key using a tuple of combination information.
        mykey = (subject,split_conditon)

        # If combination already present just append the stat to the list.
        if mykey in combination_stat_lookup:
            combination_stat_lookup[mykey].append(stat)
        else:
            combination_stat_lookup[mykey] = [stat]

    # Now iterate the the combination dictionary to calculate the required stat for a unique combination.
    for key,values in combination_stat_lookup.items():
        # Save information from key in understandable variables.
        my_subject,my_split_condition = key

        # Retrive the split column and value from the split condition.
        my_split_col, my_split_value = split_lookup[my_split_condition]

        # Apply WHERE to filter the df on Split condition and split value.
        filter_df = df.where(df[my_split_col] == my_split_value)

        # Apply GROUPBY based the provided subject.
        grouped_df = filter_df.groupby(my_subject)['AB','HBP', 'H', 'BB', 'SF', 'TB','PA'].sum()

        # Remove the rows with PA more than 25.
        final_df = grouped_df[grouped_df['PA'] >= 25]

        # Set new columns based on the requirements of the output format.
        final_df.loc[:,'SubjectId'] = final_df.index
        final_df['SubjectId'] = final_df.loc[:,'SubjectId'].apply(int)
        final_df.loc[:, 'Split'] = my_split_condition
        final_df.loc[:, 'Subject'] = my_subject

        # Calculate 'Value' column for each stat.
        # Use of 'IF..ELSE' is required due to the possibility of absence of any stat for a given unique combination.
        for my_stat in values:
            temp_df = final_df.copy()
            if my_stat == 'AVG':
                temp_df.loc[:,'Value'] = final_df.apply(getAVG, axis=1)
            elif my_stat == 'OBP':
                temp_df.loc[:, 'Value'] = final_df.apply(getOBP, axis=1)
            elif my_stat == 'SLG':
                temp_df.loc[:, 'Value'] = final_df.apply(getSLG, axis=1)
            elif my_stat == 'OPS':
                temp_df.loc[:, 'Value'] = final_df.apply(getOPS, axis=1)

            # Add the Stat column specific to the current stat
            temp_df.loc[:, 'Stat'] = my_stat

            # Truncate the temp_df to required columns only
            temp_df = temp_df[['SubjectId', 'Stat', 'Split', 'Subject', 'Value']]

            # Append the result of the current stat to the result collection.
            collection_df.append(temp_df)

    # Join all the results present in the collection.
    result_df = pd.concat(collection_df, axis=0, ignore_index=True).sort_values(['SubjectId', 'Stat', 'Split', 'Subject'],
                                                                        ascending=True)

    # Save the dataframe to a CSV file.
    result_df.to_csv('./data/processed/output.csv', index=False)

if __name__ == '__main__':
    main()